import {web_component} from "../controller/web_component.js";
import {indicator} from "./INDUS_conception_template.js";
import {Chart, registerables} from 'chart.js';
Chart.register(...registerables);

export class INDUS_indicator extends web_component {
    constructor() {
        super();
    }

    init_template() {
        this.appendChild(web_component.hydrate_and_parse_template(indicator, {
            indicator: 'Indicateurs'
        }));

        /**
         * ChartJS
         * */
        let todo = 0;
        let working = 0;
        let done = 0;

        window.tickets.forEach((el) => {
            (el.status === "todo") ? todo += 1 : 0;
            (el.status === "working") ? working += 1 : 0;
            (el.status === "done") ? done += 1 : 0;
        })

        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
                label: "Status des tickets",
                labels: ["Fait", "En cours", "À faire"],
                datasets: [{
                    backgroundColor: ["#3cba9f", "#8e5ea2", "#3e95cd"],
                    data: [done, working, todo]
                }]
            },
            options: {
                plugins: {
                    legend: {
                        display: true,
                        labels: {
                            color: 'rgb(255, 255, 255)',
                            font: {
                                size: 40
                            },
                            padding: 80
                        }
                    }
                }
            }
        });
    }
}

let customElementRegistry = window.customElements;
customElementRegistry.define("indus-indicator", INDUS_indicator);
