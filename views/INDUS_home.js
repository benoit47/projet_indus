import {web_component} from "../controller/web_component.js";
import {home, home_line_ticket} from "./INDUS_conception_template.js";

export class INDUS_home extends web_component {
    constructor() {
        super();
        this.validated = 0;
        this.tickets = [
            {
                id: 1,
                status: "done",
                priority: "1",
                label: "Mon ordinateur ne s'allume plus",
                autor: "Benoit",
            },
            {
                id: 2,
                status: "working",
                priority: "2",
                label: "Ça marchait avant",
                autor: "Dimitri",
            },
            {
                id: 3,
                status: "todo",
                priority: "3",
                label: "Ça ne marche pas sur ce pc",
                autor: "Anthony",
            },
            {
                id: 4,
                status: "done",
                priority: "4",
                label: "Heureusement, il y a Findus. Findus!!!",
                autor: "Léo",
            },
        ];
        window.tickets = this.tickets;
    }

    /**
     * Init home template and load tickets
     */
    init_template() {
        let html = "";
        for (const ticket of window.tickets) {
            html += web_component.hydrate_template(home_line_ticket, {
                ticket_id: ticket.id,
                ticket_status:
                    ticket.status === "done"
                        ? "Fait"
                        : ticket.status === "working"
                            ? "En cours"
                            : ticket.status === "todo"
                                ? "À faire"
                                : "",
                ticket_priority: ticket.priority,
                ticket_autor: ticket.autor,
                ticket_label: ticket.label,
            });
        }
        this.appendChild(web_component.parse_as_html(home));
        document.getElementById("ticket_lines").appendChild(web_component.parse_as_html(html));
        this.init_event();
    }

    /**
     * Create all event
     */
    init_event() {

        //Add tickets event
        document.getElementById("add_ticket").addEventListener("click", async () => {
            this.set_event(null);
        });

        //Add event for each ticket line
        for (let ticket of window.tickets) {
            document.getElementById("ticket_" + ticket.id).querySelector(".ticket_line_edit").querySelector(".icon").addEventListener("click", async () => {
                this.set_event(ticket);
            });
        }
    }

    /**
     * Set event
     * @param ticket
     */
    set_event(ticket) {
        web_component.delete_child(document.querySelector("indus-upsert-ticket"));
        window.current_ticket = ticket;
        document.querySelector("indus-upsert-ticket").init_template();
        document.querySelector("indus-upsert-ticket").style.display = "flex";
        document.querySelector("indus-home").style.display = "none";
    }
}

let customElementRegistry = window.customElements;
customElementRegistry.define("indus-home", INDUS_home);
