import {web_component} from "../controller/web_component.js";
import {login} from "./INDUS_conception_template.js";

export class INDUS_login extends web_component {
    constructor() {
        super();
    }

    init_template() {
        this.appendChild(web_component.hydrate_and_parse_template(login, {
            technicien: 'Technicien',
            admin: 'Admin'
        }));
        this.init_event();
    }

    /**
    * Init all event of this view
    * */
    init_event(){
        document.getElementById('select_choice_login').addEventListener('change', ()=>{
            document.querySelector('.header_right').lastChild.textContent = document.getElementById('select_choice_login').value;
        })
    }
}

let customElementRegistry = window.customElements;
customElementRegistry.define('indus-login', INDUS_login);
