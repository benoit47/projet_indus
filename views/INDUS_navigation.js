import {web_component} from "../controller/web_component.js";
import {navigation} from "./INDUS_conception_template.js";

export class INDUS_navigation extends web_component {
    constructor() {
        super();
    }

    init_template() {
        let html = '';
        let i = 0;
        let array_title = ['Accueil', 'Planning', 'Indicateurs'];
        (document.querySelector('.header_right').lastChild.textContent === 'Technicien') ? 0 : array_title.push('Administration');
        for (let el in array_title) {
            html += web_component.hydrate_template(navigation, {
                active_class: (i === 0) ? "active first" : "",
                number_element: i,
                title: array_title[el]
            });
            i++;
        }
        this.appendChild(web_component.parse_as_html(html));
        this.add_event_listener(window.view_controller.menu_selected)
    }
}

let customElementRegistry = window.customElements;
customElementRegistry.define('indus-navigation', INDUS_navigation);
