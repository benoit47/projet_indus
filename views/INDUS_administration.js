import {web_component} from "../controller/web_component.js";
import {
    administration,
    administration_popup_delete,
    administration_popup_update,
    template_user_administration
} from "./INDUS_conception_template.js";

export class INDUS_administration extends web_component {
    constructor() {
        super();
        this.validated = 0;
        this.array_users = [{
            firstname: 'Dimitry',
            secondname: 'VINCENT',
            mail: 'm.exemple@indus.fr',
            fonction: 'Technicien'
        }, {
            firstname: 'Léo',
            secondname: 'LAURENT',
            mail: 'm.exemple@indus.fr',
            fonction: 'Technicien'
        }, {
            firstname: 'Anthony',
            secondname: 'KELLER',
            mail: 'm.exemple@indus.fr',
            fonction: 'Technicien'
        }, {
            firstname: 'Benoit',
            secondname: 'LOUVEAU',
            mail: 'm.exemple@indus.fr',
            fonction: 'Technicien'
        }];
    }

    init_template() {
        //init the title
        let html = web_component.hydrate_template(administration, {
            title: "Administration",
            src: "../ressources/indus_add.png"
        });

        //init the array of user
        for (let el in this.array_users) {
            html += web_component.hydrate_template(template_user_administration, {
                index: el,
                firstname: this.array_users[el].firstname,
                secondname: this.array_users[el].secondname,
                edit: '',
                delete: ''
            });
        }
        this.appendChild(web_component.parse_as_html(html)); //appendChild is natif to javascript
        this.init_event();
    }

    /**
    * init all event of this view
    * */
    init_event() {

        /**
         * Event for add user
         * */
        document.getElementById('administration_img_add').addEventListener('click', async () => {
            await this.create_popup_add();
        });

        /**
         * Event on each line of user
         * */
        let array_html = document.querySelectorAll(".administration_line_user");
        for (let el of array_html) {
            el.querySelector('.administration_column_edit').addEventListener('click', async (value) => {
                await this.create_popup_update(el);
            });
            el.querySelector('.administration_column_delete').addEventListener('click', async (value) => {
                await this.create_popup_delete(el);
            });
        }
    }

    /**
     * Function for create each popup
     * */
    async create_popup_add() {
        let html = document.createElement('div').innerHTML = web_component.hydrate_template(administration_popup_update, {
            title: 'Création utilisateur',
            text_firstname: 'Nom',
            place_holder_firstname: "Nom de l'utilisateur",
            text_secondname: 'Prénom',
            place_holder_secondname: "Prénom de l'utilisateur",
            text_email: 'Mail',
            place_holder_mail: "Mail de l'utilisateur",
            text_function: 'Fonction',
            technicien: "Technicien",
            admin: "Administrateur",
            firstname: '',
            secondname: '',
            mail: ''
        });
        document.body.appendChild(web_component.parse_as_html(html));
        document.getElementById('administration_popup_update_confirm').addEventListener('click', async () => {
            await this.add_user_to_array().then(() => {
                if (this.validated === 1) {
                    web_component.delete(document.querySelector(".administration_popup"));
                }
            })
        });
    }

    /**
     * Creation of pop-up to delete element
     * @param el
     * @returns {Promise<void>}
     */
    async create_popup_delete(el) {
        let html = document.createElement('div').innerHTML = web_component.hydrate_template(administration_popup_delete, {
            title: "Suppression utilisateur",
            text: "Confirmez-vous la suppression de l'utilisateur ?"
        });
        document.body.appendChild(web_component.parse_as_html(html));
        document.getElementById('administration_popup_delete_confirm').addEventListener('click', () => {
            web_component.delete(document.querySelector(".administration_popup"));
            web_component.delete(el);
        });
        document.getElementById('administration_popup_delete_cancel').addEventListener('click', () => {
            web_component.delete(document.querySelector(".administration_popup"));
        });
    }

    /**
     * Creation of pop-up to update element
     * @param el
     * @returns {Promise<void>}
     */
    async create_popup_update(el) {
        let i = el.getAttribute("index-element");
        let html = document.createElement('div').innerHTML = web_component.hydrate_template(administration_popup_update, {
            title: 'Edition utilisateur',
            text_firstname: 'Nom',
            firstname: this.array_users[i].firstname,
            text_secondname: 'Prénom',
            secondname: this.array_users[i].secondname,
            text_email: 'Email',
            mail: this.array_users[i].mail,
            text_function: 'Fonction',
            technicien: 'Technicien',
            admin: 'Administrateur',
            checked_technicien: (this.array_users[i].fonction === 'Technicien') ? 'checked' : '',
            checked_admin: (this.array_users[i].fonction === 'Administrateur') ? 'checked' : ''
        });
        document.body.appendChild(web_component.parse_as_html(html));
        document.getElementById('administration_popup_update_confirm').addEventListener('click', async () => {
            await this.search_element_to_update(i, el);
            web_component.delete(document.querySelector(".administration_popup"));
        });
    }

    /**
     * Function using for functionality of each popup
     * */
    async add_user_to_array() {
        let firstname = document.getElementById('text_firstname').value;
        let secondname = document.getElementById('text_secondname').value;
        let mail = document.getElementById('text_email').value;
        let fonction = '';
        if (document.querySelector('input[name="radio"]:checked')) {
            fonction = document.querySelector('input[name="radio"]:checked').value
        }

        this.check_all_form_value().then(() => {
            (this.validated === 1) ? this.update_html_user_array(firstname, secondname, mail, fonction) : 0;
        })
    }

    /**
     * Update the html
     * @param firstname
     * @param secondname
     * @param mail
     * @param fonction
     * @returns {Promise<void>}
     */
    async update_html_user_array(firstname, secondname, mail, fonction) {
        /**
         * update user array
         * */
        this.array_users.push({
            firstname: firstname,
            secondname: secondname,
            mail: mail,
            fonction: fonction
        });

        /**
         * update html
         * */
        let array = document.querySelectorAll(".administration_line_user");
        let i = [];
        for (let el of array) {
            i.push(el.getAttribute("index-element"));
        }
        let last_value = parseInt(Math.max(...i))
        let html = web_component.hydrate_template(template_user_administration, {
            index: last_value + 1,
            firstname: this.array_users[this.array_users.length - 1].firstname,
            secondname: this.array_users[this.array_users.length - 1].secondname,
            edit: '',
            delete: ''
        });

        /**
         * add the new html element to the list
         * */
        document.querySelector('indus-administration').appendChild(web_component.parse_as_html(html));

        /**
         * Bind event to the new html element
         * */
        let last_html = document.querySelectorAll(".administration_line_user");
        last_html = last_html[last_html.length - 1];
        last_html.querySelector('.administration_column_edit').addEventListener('click', async (value) => {
            await this.create_popup_update(last_html);
        });
        last_html.querySelector('.administration_column_delete').addEventListener('click', async (value) => {
            await this.create_popup_delete(last_html);
        });
    }


    /**
     * Check if all value are filled
     * */
    async check_all_form_value() {
        let verif_firstname = (document.getElementById('text_firstname').value !== '') ? 1 : 0;
        let verif_secondname = (document.getElementById('text_secondname').value !== '') ? 1 : 0;
        let verif_mail = (document.getElementById('text_email').value !== '') ? 1 : 0;
        let verif_fonction = (document.querySelector('input[name="radio"]:checked')) ? 1 : 0;
        (verif_firstname === 1 && verif_secondname === 1 && verif_mail === 1 && verif_fonction === 1) ? this.validated = 1 : this.validated = 0;
        (this.validated === 0) ? alert("Veuillez remplir tout les champs") : 0;
    }

    /**
     * Search element to update
     * @param i
     * @param el
     * @returns {Promise<void>}
     */
    async search_element_to_update(i, el) {
        let firstname = document.getElementById('text_firstname').value;
        let secondname = document.getElementById('text_secondname').value;
        let mail = document.getElementById('text_email').value;
        let fonction = document.querySelector('input[name="radio"]:checked').value;
        await this.parser_array_user(el, i, firstname, secondname, mail, fonction);
    }

    /**
     * Parse array
     * @param el
     * @param id
     * @param firstname
     * @param secondname
     * @param mail
     * @param fonction
     * @returns {Promise<void>}
     */
    async parser_array_user(el, id, firstname, secondname, mail, fonction) {
        this.array_users[id].firstname = firstname;
        this.array_users[id].secondname = secondname;
        this.array_users[id].mail = mail;
        this.array_users[id].fonction = fonction;
        await this.update_user_array_html(el, firstname, secondname, mail, fonction);
    }

    /**
     * update array
     * @param el
     * @param firstname
     * @param secondname
     * @returns {Promise<void>}
     */
    async update_user_array_html(el, firstname, secondname) {
        el.querySelector('.administration_column_firstname').textContent = firstname;
        el.querySelector('.administration_column_secondname').textContent = secondname;
    }
}

let customElementRegistry = window.customElements;
customElementRegistry.define('indus-administration', INDUS_administration);
