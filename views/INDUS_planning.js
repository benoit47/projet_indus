import {web_component} from "../controller/web_component.js";
import {planning} from "./INDUS_conception_template.js";

export class INDUS_planning extends web_component {
    constructor() {
        super();
    }

    init_template() {
        this.appendChild(web_component.hydrate_and_parse_template(planning, {
            planing: 'Planing'
        }));
    }
}

let customElementRegistry = window.customElements;
customElementRegistry.define("indus-planning", INDUS_planning);
