import {web_component} from "../controller/web_component.js";
import {header} from "./INDUS_conception_template.js";

export class INDUS_header extends web_component {
    constructor() {
        super();
    }

    init_template() {
        this.appendChild(web_component.hydrate_and_parse_template(header, {
            project_name: 'Findus',
            technicien: 'Technicien',
        }));
    }
}

let customElementRegistry = window.customElements;
customElementRegistry.define('indus-header', INDUS_header);
