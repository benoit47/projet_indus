let header = `
<div class="header_left"><div><img class="indus_logo" src="../ressources/indus_logo.png">
     {{project_name}}</div></div>
<div class="header_right"><img class="indus_user" src="../ressources/indus_user.png"><div>{{technicien}}</div></div>
`;

let login = `
<select id="select_choice_login">
    <option value="{{technicien}}" id="option_technicien">{{technicien}}</option>
    <option value="{{admin}}" id="option_admin">{{admin}}</option>
</select>
<input type="button" class="btn_valid_login" id="btn_valid_login" value="Valider">
`;

let navigation = `
    <div class="{{active_class}}"  indexe-element="{{number_element}}">
        <p class="textSelector">{{title}}</p>
    </div>
`;

let home = `
<div class="headband">
    <h1>Accueil</h1>
    <img class="icon" id="add_ticket" src="/ressources/INDUS_add.png" alt="add ticket">
</div>
<div>  
    <table>
        <thead>
        <tr>
            <th>Action</th>
            <th>Id</th>
            <th>Status</th>
            <th>Priorité</th>
            <th>Technicien</th>
            <th>Libellé</th>
        </tr>
        </thead>
        <tbody id="ticket_lines">
        </tbody> 
    </table>
</div>
`;

let home_line_ticket = `
<tr id="ticket_{{ticket_id}}">
    <td class="ticket_line_edit"><img class="icon" src="../ressources/indus_edit.png" alt:"edit ticket"></td>
    <td class="ticket_line_id">{{ticket_id}}</td>
    <td class="ticket_line_status">{{ticket_status}}</td>
    <td class="ticket_line_priority">{{ticket_priority}}</td>
    <td class="ticket_line_user">{{ticket_autor}}</td>
    <td class="ticket_line_label">{{ticket_label}}</td>
</tr>
`;

let administration = `
<div class="administration_div_title">
    <h1 class="administration_title">{{title}}</h1>
    <img id="administration_img_add" class="icon" src="{{src}}"/>
</div>
`;
let template_user_administration = `
<div class="administration_line_user" index-element="{{index}}">
    <div class="administration_column_firstname">{{firstname}}</div>
    <div class="administration_column_secondname">{{secondname}}</div>
    <div class="administration_column_edit"><img class="icon" src="../ressources/indus_edit.png"/></div>
    <div class="administration_column_delete"><img class="icon" src="../ressources/indus_delete.png"/></div>
</div>
`;
let administration_popup_delete = `
<div class="administration_popup">
    <div class="administration_popup_delete">
        <h1>{{title}}</h1>
        <p>{{text}}</p>
        <div class="administration_popup_delete_btn">
            <input type="button" value="VALIDER" id="administration_popup_delete_confirm" class="administration_popup_delete_confirm">
            <input type="button" value="REFUSER" id="administration_popup_delete_cancel" class="administration_popup_delete_cancel">
        </div>
    </div>
</div>
`;

let administration_popup_update = `
<div class="administration_popup">
    <div class="administration_popup_update">
        <h1>{{title}}</h1>
        
        <p>{{text_firstname}}</p>
        <input id="text_firstname" type="text" value="{{firstname}}" placeholder="{{place_holder_firstname}}">
     
        <p>{{text_secondname}}</p>
        <input id="text_secondname" type="text" value="{{secondname}}" placeholder="{{place_holder_secondname}}">
        
        <p>{{text_email}}</p>
        <input id="text_email" type="text" value="{{mail}}" placeholder="{{place_holder_mail}}">
        
        <p>{{text_function}}</p>
        
        <div class="administration_popup_update_radio">
            <div><input type="radio" id="administration_popup_update_radio_technicien" name="radio" value="{{technicien}}" {{checked_technicien}}>
            <label for="administration_popup_update_radio_technicien">{{technicien}}</label></div>
            
            <div><input type="radio" id="administration_popup_update_radio_admin" name="radio" value="{{admin}}" {{checked_admin}}>
            <label for="administration_popup_update_radio_admin">{{admin}}</label></div>
        </div>

        
        <input type="button" value="VALIDER" id="administration_popup_update_confirm" class="administration_popup_update_confirm">

    </div>
</div>
`;

let upsert_ticket = `
<div>
    <div>
    <h1>{{ticket_action}}</h1>
    <p>{{ticket_label_label}}</p>
    <input
        id="input_ticket_label"
        type="text"
        value="{{ticket_label_value}}"
        placeholder="{{ticket_label_place_holder}}"
    />

    <p>{{ticket_status_label}}</p>
    <select id="select_ticket_status">
        <option value="todo">À faire</option>
        <option value="working">En cours</option>
        <option value="done">Fait</option>
    </select>

    <p>{{ticket_priority_label}}</p>
    <select id="select_ticket_priority">
        <option value="1" >1</option>
        <option value="2" >2</option>
        <option value="3" >3</option>
        <option value="4" >4</option>
        <option value="5" >5</option>
    </select>

    <p>{{ticket_autor_label}}</p>
    <input
        id="input_ticket_autor"
        type="text"
        value="{{ticket_autor_value}}"
        placeholder="{{ticket_autor_place_holder}}"
    />

    <p>{{ticket_subject_label}}</p>
    <textarea id="textaera_ticket_subject"  rows="5" cols="33">
    </textarea
    >
    </div>
<button id="upsert_ticket_validate">Valider</button>
<button id="upsert_ticket_cancel">Annuler</button>
</div>
`;

let planning = `
{{planning}}
`;

let indicator = `
<canvas id="pie-chart" width="800px" height="450px"></canvas>
`;

export {
  header,
  login,
  navigation,
  administration,
  template_user_administration,
  administration_popup_delete,
  administration_popup_update,
  home,
  home_line_ticket,
  upsert_ticket,
  planning,
  indicator
};
