import {web_component} from "../controller/web_component.js";
import {upsert_ticket} from "./INDUS_conception_template.js";

export class INDUS_upsert_ticket extends web_component {
    constructor() {
        super();
    }

    /**
     * Init upsert_ticket template
     */
    init_template() {
        window.current_ticket;
        this.appendChild(
            web_component.hydrate_and_parse_template(upsert_ticket, {
                ticket_action: window.current_ticket
                    ? `Mise à jour du ticket ${window.current_ticket.id}`
                    : "Création d'un ticket",
                ticket_label_label: "Label",
                ticket_label_value: window.current_ticket
                    ? window.current_ticket.label
                    : "",
                ticket_label_place_holder: "Label du ticket",
                ticket_status_label: "Statut du ticket",
                ticket_priority_label: "Priorité du ticket",
                ticket_autor_label: "Auteur du ticket",
                ticket_autor_value: window.current_ticket
                    ? window.current_ticket.autor
                    : "",
                ticket_autor_place_holder: "Le nom de l'auteur du ticket",
                ticket_subject_label: "Sujet du ticket",
            })
        );
        document.getElementById("select_ticket_status").value =
            window.current_ticket ? window.current_ticket.status : "todo";
        document.getElementById("select_ticket_priority").value =
            window.current_ticket ? window.current_ticket.priority : "1";
        document.getElementById("textaera_ticket_subject").value =
            window.current_ticket
                ? window.current_ticket.subject
                    ? window.current_ticket.subject
                    : ""
                : "";

        this.init_event();
    }


    /**
     * init all event of this view
     * */
    init_event() {

        //Add validate event
        document
            .getElementById("upsert_ticket_validate")
            .addEventListener("click", async () => {

                //add validate form here WIP
                this.upsert_ticket({
                    //get id from object if exist else get max id of windows.tickets + 1 (=> replace by id from BDD insert WIP)
                    id: window.current_ticket
                        ? window.current_ticket.id
                        : Math.max(...window.tickets.map((user) => user.id)) + 1,
                    //get value from form
                    status: document.getElementById("select_ticket_status").value,
                    priority: document.getElementById("select_ticket_priority").value,
                    label: document.getElementById("input_ticket_label").value,
                    autor: document.getElementById("input_ticket_autor").value,
                    subject: document.getElementById("textaera_ticket_subject").value,
                });

                web_component.reload("indus-home");
                this.close_upsert_ticket();
            });

        //Add cancel event
        document
            .getElementById("upsert_ticket_cancel")
            .addEventListener("click", async () => {
               this.close_upsert_ticket();
            });
    }

    /**
     * close upsert-ticket
     */
    close_upsert_ticket() {
        document.querySelector("indus-upsert-ticket").style.display = "none";
        document.querySelector("indus-home").style.display = "flex";
    }

    /**
     * Add or update ticket to window.tickets (Replace by upsert in BDD WIP)
     * @param {*} item
     */
    upsert_ticket(item) {
        const i = window.tickets.findIndex((_item) => _item.id === item.id);
        if (i > -1) window.tickets[i] = item;
        else window.tickets.push(item);
    }
}

let customElementRegistry = window.customElements;
customElementRegistry.define("indus-upsert-ticket", INDUS_upsert_ticket);
