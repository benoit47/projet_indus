/**
 * All javascript files are imported there
 * */

//region import
import {view_controller} from "./view_controller.js";
import {INDUS_header} from "../views/INDUS_header.js";
import {INDUS_login} from "../views/INDUS_login.js";
import {INDUS_navigation} from "../views/INDUS_navigation.js";
import {INDUS_home} from "../views/INDUS_home.js";
import {INDUS_administration} from "../views/INDUS_administration.js";
import {INDUS_planning} from "../views/INDUS_planning.js";
import {INDUS_indicator} from "../views/INDUS_indicator.js";
import {INDUS_upsert_ticket} from "../views/INDUS_upsert_ticket.js";
//endregion import

let View_controller = new view_controller();
init();

/**
 * init application
 * */
async function init(){
    await View_controller.load_async();
}
