import {web_component} from "./web_component.js";

export class view_controller {
    constructor() {
    }

    /**
     * async function for loading the view
     * */
    async load_async() {
        await this.init_template(); //Call for loading the view
    }

    /**
     * async function for generate views
     * initialization of events at the end of the function
     * */
    async init_template() {

        //Initializing views
        this.INDUS_header = document.getElementsByTagName("indus-header")[0];
        this.INDUS_header.init_template();

        this.INDUS_Login = document.getElementsByTagName("indus-login")[0];
        this.INDUS_Login.init_template();

        window.view_controller = this;

        // Initialization of events
        this.add_event();
        this.add_listener();
        this.register_listener();
    }

    /**
     * Events on views
     * */
    add_event() {
        document.getElementById("btn_valid_login").addEventListener("click", () => {

            let html = document.querySelector("indus-login");
            web_component.delete(html);

            //init for navigation view
            this.INDUS_navigation = document.getElementsByTagName("indus-navigation")[0];
            this.INDUS_navigation.init_template();

            //init for home view
            this.INDUS_home = document.getElementsByTagName("indus-home")[0];
            this.INDUS_home.init_template();

            //init for indicator view
            this.INDUS_indicator = document.getElementsByTagName("indus-indicator")[0];
            this.INDUS_indicator.init_template();

            //init for administration view
            this.INDUS_Administration = document.getElementsByTagName("indus-administration")[0];
            this.INDUS_Administration.init_template();
        });
    }

    /**
     * Getter Setter for events
     * */
    add_listener() {

        //Getter Setter events for each menu selected
        this.menu_selected = {
            value_internal: 0,
            value_listener: function (val) {
            },
            set value(val) {
                this.value_internal = val;
                this.value_listener(val);
            },
            get value() {
                return this.value_internal;
            },
            register_listener: function (listener) {
                this.value_listener = listener;
            },
        };
    }


    /**
     * Control views for each selected element
     * */
    register_listener() {
        this.menu_selected.register_listener(() => {
            switch (this.menu_selected.value) {
                case 0 : //Home
                    document.querySelector('indus-home').style.display = 'flex';
                    document.querySelector('indus-planning').style.display = 'none';
                    document.querySelector('indus-indicator').style.display = 'none';
                    document.querySelector('indus-administration').style.display = 'none';
                    break;
                case 1 : //Planning
                    document.querySelector('indus-home').style.display = 'none';
                    document.querySelector('indus-planning').style.display = 'flex';
                    document.querySelector('indus-indicator').style.display = 'none';
                    document.querySelector('indus-administration').style.display = 'none';
                    break;
                case 2 : //Indicator
                    document.querySelector('indus-home').style.display = 'none';
                    document.querySelector('indus-planning').style.display = 'none';
                    document.querySelector('indus-indicator').style.display = 'flex';
                    document.querySelector('indus-administration').style.display = 'none';
                    break;
                case 3 : //Administration
                    document.querySelector('indus-home').style.display = 'none';
                    document.querySelector('indus-planning').style.display = 'none';
                    document.querySelector('indus-indicator').style.display = 'noe';
                    document.querySelector('indus-administration').style.display = 'flex';
                    break;
            }
        });
    }
}
