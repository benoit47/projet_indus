/**
 * @class web_component
 */
export class web_component extends HTMLElement { //Cannot rename HTMLElement, it's a web element

  constructor() {
    super();
    this.is_visible = true;
  }

  /**
   * hide a web_component
   */
  hide() {
    this.is_visible = false;
    this.style.visibility = "hidden";
  }

  /**
   * show a web_component
   */
  show() {
    this.is_visible = true;
    this.style.visibility = "visible";
  }

  /**
   * Hide children of a HTML element with id
   * @param {string} element
   */
  static kill_children_html(element) {
    [].slice.call(document.getElementById(element).children).forEach((el) => {
      el.remove();
    });
  }

  /**
   * hide HTML element
   * @param {string} id
   */
  static hide_html(id) {
    if (document.getElementById(id)) {
      document.getElementById(id).style.display = "none";
    }
  }

  /**
   * show HTML element
   * @param {string} id
   */
  static show_html(id) {
    if (document.getElementById(id)) {
      document.getElementById(id).style.display = "flex";
    }
  }

  /**
   * Hide all children of html element
   * @param {string} id
   */
  static hide_recursif(id) {
    let parent = document.getElementById(id).parentNode;
    let number_of_children = parent.children.length;
    for (let i = 0; i < number_of_children; i++) {
      document.getElementById(parent.children[i].id).style.display = "none";
    }
  }

  /**
   * load HTML template
   * @param {string} url
   * @returns {Promise<string>}
   */
  static async load_template(url) {
    return fetch(url)
      .then(function (response) {
        return response.text();
      })
      .then(function (html) {
        return html;
      });
  }

  /**
   * Hydrates an HTML template as a string
   * @param {string} template
   * @param {Object} object
   * @returns {*}
   */
  static hydrate_template(template, object = {}) {
    for (let element_to_hydrate in object) {
      template = template.replaceAll( //replaceAll is natif, can't be change
        `{{${element_to_hydrate}}}`,
        object[element_to_hydrate]
      );
    }
    return template;
  }

  /**
   * Convert a string to HTML
   * @param {string} html
   * @returns {DocumentFragment}
   */
  static parse_as_html(html) {
    let template = document.createElement("template");
    template.innerHTML = html;
    return template.content;
  }

  /**
   * Load and hydrate an HTML template
   * @param {string} url
   * @param {Object} object
   * @returns {Promise<*>}
   */
  static async load_and_hydrate_template(url, object) {
    let html = await web_component.load_template(url);
    return web_component.hydrate_template(html, object);
  }

  /**
   * Load, Hydrate and Convert an HTML template
   * @param {string} url
   * @param {Object} object
   * @returns {Promise<DocumentFragment>}
   */
  static async load_hydrate_and_parse_template(url, object = {}) {
    let html = await web_component.load_template(url);
    return web_component.parse_as_html(
      web_component.hydrate_template(html, object)
    );
  }

  /**
   * Hydrates and Converts an HTML template
   * @param {string} html
   * @param {Object} object
   * @returns {DocumentFragment}
   */
  static hydrate_and_parse_template(html, object = {}) {
    return web_component.parse_as_html(
      web_component.hydrate_template(html, object)
    );
  }

  /**
   * Deletes an element and all of its children
   * @param {HTMLElement} elements
   */
  static delete(elements) {
    let array = [];
    array.push(elements);
    this.delete_recursive(elements, array);
    for (let html of array) {
      html.remove();
    }
  }

  /**
   * Delete elements recursively until there are no more children
   * @param {HTMLElement} elements
   * @param {array} array
   */
  static delete_recursive(elements, array) {
    if (elements) {
      for (let elem of elements.children) {
        array.push(elem);
        web_component.delete_recursive(elem, array);
      }
    }
  }

  /**
   * Delete child
   * @param {HTMLElement} elements
   */
  static delete_child(elements) {
    let first = elements.firstElementChild;
    while (first) {
      first.remove();
      first = elements.firstElementChild;
    }
  }

  /**
   * Add event to listener
   * @param listenerController
   */
  add_event_listener(listener_controller) {
    let array = [...this.querySelectorAll("div")];
    let length = array.length;
    while (length--) {
      let indexe = length;
      array[indexe].addEventListener("click", (event) => { // can't change addEventListener it's javascript component
        this.selected = indexe;
        listener_controller.value = indexe;
        this.change_choice_selector(array, indexe);
      });
    }
  }
  /**
   * add class to selected item
   * @param arrayChoices
   * @param indexChoice
   */
  change_choice_selector(arrayChoices, indexChoice) {
    arrayChoices.forEach((item) => {
      item.classList.remove("active");
    });
    arrayChoices[indexChoice].classList.add("active");
  }

  /**
   * Delete and recreate element
   * @param {*} element
   */
  static reload(element) {
    this.delete_child(document.querySelector(element));
    document.querySelector(element).init_template();
  }
}
