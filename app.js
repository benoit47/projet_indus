let Express = require("express");
let app = Express();
let server = app.listen(80, () => {
    console.log("Listening on port " + server.address().port);
});
module.exports = server;
